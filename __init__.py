""" Small utility for removing files with file storage support(with restoring functionality)

Run the next command to start installation:
	python setup.py install
or
	pip install ./(path to package)

The utility consists of the next modules:
	-argparser - working with console arguments
	-config - working with configuration files
	-recycler - responsible for removing operations
	-trashbin - implements file storage functionality
	-helpers - some auxiliary functions
"""
