#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys
import shutil
import os
import csv
import unittest

from rmtool import helpers
from rmtool import constants
from rmtool.trashbin import Trashbin
from rmtool.recycler import Recycler

class TestRecyclerMethods(unittest.TestCase):

    def setUp(self):
        self.storage_path = "TESTBIN"
        self.recycler = Recycler(recursive_mode=True, 
                                 permanent_mode=False,
                                 path_to_storage=self.storage_path)

        self.trashbin = Trashbin(path_to_storage=self.storage_path)
        self.paths = ["tf1", "tf2", "tf3", "tf4"]
        self.items = []
          
    def test_remove_recursive(self): 
        for path in self.paths:
            os.makedirs(os.path.join(path, "folder"), 0700)

        for path in self.paths:
            self.items.append(self.recycler.remove(path))
            self.assertFalse(os.path.exists(path))
        for result in self.items:
            for errors, source, dest in result:
                self.assertEqual(errors[0], 0)
        self.assertEqual(self.trashbin.count_items(), 4)
        self.assertFalse(self.trashbin.check_is_empty())

    def test_remove_dir(self):
        self.recycler.recursive_mode = False
        for path in self.paths:
            os.makedirs(path, 0700)

        for path in self.paths:
            self.items.append(self.recycler.remove(path))
            self.assertFalse(os.path.exists(path))

        for result in self.items:
            for errors, source, dest in result:
                self.assertEqual(errors[0], 0)

        self.assertEqual(self.trashbin.count_items(), 4)
        self.assertFalse(self.trashbin.check_is_empty())

    def test_storage_overflow(self):
        for path in self.paths:
            with open(path, "w") as f:
                f.seek(11000000)
                f.write('\0')   
        self.recycler.storage_size = 35

        for path in self.paths:
            self.items.append(self.recycler.remove(path))

        self.assertFalse(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 3)

    def test_too_big_file(self):
        for path in self.paths:
            with open(path, "w") as f:
                f.seek(9000000)
                f.write('\0')
                
        self.recycler.storage_size = 8

        for path in self.paths:
            self.items.append(self.recycler.remove(path))

        for result in self.items:
            for errors, source, dest in result:
                self.assertNotEqual(errors[0], 0)

        self.assertTrue(self.trashbin.check_is_empty())
        
    def test_restrictions_access(self):
        self.recycler.recursive_mode = False

        for path in self.paths:
            os.makedirs(path, 0500)

        for path in self.paths:
            self.recycler.remove(path)
            self.assertTrue(os.path.exists(path))

        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)
        
    def test_restrictions_empty_folder(self):
        self.recycler.recursive_mode = False

        for path in self.paths:
            os.makedirs(os.path.join(path, "folder"), 0700)

        for path in self.paths:
            self.recycler.remove(path)
            self.assertTrue(os.path.exists(path))
            
        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)

    def test_remove_by_regexp(self):
        for path in self.paths:
            os.makedirs(os.path.join(path, "folder"), 0700)

        self.items.append(self.recycler.remove_by_regexp("^tf", "."))

        for result in self.items:
            for errors, source, dest in result:
                self.assertEqual(errors[0], 0)
        self.assertEqual(self.trashbin.count_items(), 4)

    def test_remove_link(self):
        os.mkdir(self.paths[0])
        os.symlink(self.paths[0], "slnk")
        self.items.append(self.recycler.remove("slnk"))

        with open(self.trashbin.info_file_path, "r") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                self.assertEqual(line[4], "Link")

        for result in self.items:
            for errors, source, dest in result:
                self.assertEqual(errors[0], 0)

        self.assertEqual(self.trashbin.count_items(), 1)

    def test_no_matches(self):
        for path in self.paths:
            os.makedirs(os.path.join(path, "folder"), 0700)

        self.items.append(self.recycler.remove_by_regexp("^ft", "."))

        for result in self.items:
            for errors, source, dest in result:
                self.assertNotEqual(errors[0], 0)

        self.assertTrue(self.trashbin.check_is_empty())

    def test_wrong_path(self):
        self.items.append(self.recycler.remove("file"))

        for result in self.items:
            for errors, source, dest in result:
                self.assertNotEqual(errors[0], 0)
        self.assertTrue(self.trashbin.check_is_empty())

    def tearDown(self):
        for path in self.paths:
            if os.path.exists(path):
                if os.path.isdir(path):
                    shutil.rmtree(path)
                else:
                    os.remove(path)
        shutil.rmtree(self.trashbin.path_to_storage)

if __name__ == '__main__':
    unittest.main()
