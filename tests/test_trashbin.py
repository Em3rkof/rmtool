#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import shutil
import os
import unittest
import sys

from rmtool.recycler import Recycler
from rmtool.trashbin import Trashbin
from rmtool import helpers
from rmtool import constants


class TestTrashbinMethods(unittest.TestCase):
    def setUp(self):
        self.storage_path = "TESTBIN"
        self.recycler = Recycler(path_to_storage=self.storage_path)
        self.trashbin = Trashbin(path_to_storage=self.storage_path)
        self.paths = ["tf1", "tf2", "tf3", "tf4", "tf5"]
  
        for path in self.paths:
            with open(path, "w") as f:
                f.write("sample")
            self.recycler.remove(path)

    def test_creation(self):
        self.assertTrue(os.path.exists(self.trashbin.path_to_storage))
        self.assertTrue(os.path.exists(self.trashbin.info_file_path))

    def test_count_items(self):
        self.assertEqual(self.trashbin.count_items(), 5)

    def test_emptiness(self):
        self.trashbin.clean()
        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)

    def test_restore(self):
        items = []
        items = self.trashbin.restore(self.paths)
        self.assertEqual(len(items[0]), 5)
        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)

    def test_cleanup(self):
        self.trashbin.clean()
        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)

    def test_filecount_policy(self):
        self.trashbin.policy_max_file_count = 4
        self.trashbin.apply_policy()

        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)

    def test_maxsize_policy(self):
        self.trashbin.policy_max_size = 30
        self.trashbin.apply_policy()

        self.assertTrue(self.trashbin.check_is_empty())
        self.assertEqual(self.trashbin.count_items(), 0)

    def tearDown(self):
        for path in self.paths:
            if os.path.exists(path):
                os.remove(path)
        shutil.rmtree(self.trashbin.path_to_storage)

if __name__ == '__main__':
    unittest.main()
