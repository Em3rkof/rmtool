#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys
import shutil
import os
import unittest
from rmtool import helpers
from rmtool import constants


class TestHelpersMethods(unittest.TestCase):
    def setUp(self):
        os.makedirs("1/2/3/4")
        with open("1/f", "w") as f:
            f.seek(4000000)
            f.write('\0')

    def test_check_contains_files(self):
        self.assertTrue(helpers.check_contains_files("1"))

    def test_contains_dirs(self):
        self.assertTrue(helpers.check_contains_dirs("1"))

    def test_file_size(self):
        self.assertEquals(helpers.get_file_size("1"), 4)

    def tearDown(self):
        if os.path.exists("1"):
            shutil.rmtree("1")

if __name__ == '__main__':
    unittest.main()
