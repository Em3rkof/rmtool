#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import sys
import shutil
import os
import csv
import unittest

from rmtool.config import Config
from rmtool import constants

class TestConfigMethods(unittest.TestCase):
    def setUp(self):
        self.config = Config()

    def test_load_defaults(self):
        self.config.load_defaults()

        self.assertEqual(len(self.config.dictionary["bin_config"]),
                             6)
        self.assertEqual(len(self.config.dictionary["recycler_config"]),
                             5)

    def test_create_json_config(self):
        self.config.create_json_config("config", self.config.dictionary)
        self.assertTrue(os.path.exists("config.json"))
        
    def test_create_toml_config(self):
        self.config.create_toml_config("config", self.config.dictionary)
        self.assertTrue(os.path.exists("config.toml"))

    def test_load_config(self):
        self.config.load_defaults()
        self.config.create_json_config("config", self.config.dictionary)
        self.config.dictionary.clear()
        self.config.load_config("config.json")

        self.assertEqual(len(self.config.dictionary["bin_config"]),
                             6)
        self.assertEqual(len(self.config.dictionary["recycler_config"]),
                             5)        
    def tearDown(self):
        if os.path.exists("config.toml"):
            os.remove("config.toml")
        if os.path.exists("config.json"):
            os.remove("config.json")

if __name__ == '__main__':
    unittest.main()
