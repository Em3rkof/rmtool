# RMTool 0.5 

Small UNIX OS utilite providing removing/restoring your files and folders with file storage support. 
Developed on Python 2.7



## Usage guide:



To access removing facilities, run the next command in the terminal:

``` bash
rmtr [path to file] [arguments] (optional)
```

If run in default mode (e.g. without flags), removes files and *not 
empty* directories to the file storage folder.

To remove files permanently, specify *--permanent* flag:
``` bash
rmtr [path to file] --permanent
```

To remove not empty directories, specify *-r* flag:
``` bash
rmtr [path to file] -r
```

To watch how the application works without any changes being done, specify *-d* flag:
``` bash
rmtr [path to file] -d
```

**[Incompatible with -i flag!]**
If you don't want to get any console output, specify *-s* flag:
``` bash
rmtr [path to file] -s
```

**[Incompatible with -s flag!]**
If you want the application to ask for permission before any action performed, specidy *-i* flag:
``` bash
rmtr [path to fie] -i
```

If you want to configure removing procces on your own, first, get the default configure file by 
specifying --throwtoml or --throwjson file:
``` bash
rmtr [path to file] --throwtoml / --throwjson [path where you want the configuration file be created]
```

Than change values you want and run:
``` bash
rmtr [path to file] --loadtoml / --loadjson [path to your custom config]
```

To see the rest facilities, run:
``` bash
rmtr --help
```

To show file storage contents and access restoring facilities, run:
``` bash
rmtbr
```
..and follow prompts.

#### For 
```bash
rmtbr
```
#### only **-d** and **-i** flags are aviable!



## Installation guide

Run:
``` bash
pip install ~/[path to package]/rmtool
```


To uninstall package, run:
``` bash
pip uninstall RMTool
```


## Moodules description

1. **-app.py**

    Represents application's entry point. The place where all the main methods are called.

2. **-argparser.py**

    Pretty self-explanatory module. A unite, where agruments from the command line, passed to the 
    script are being processed.

3. **-config.py**

    The module provides creating/saving/loading configuration files.

4. **-setup.py**

    Specifies such information as version, software name, description and entry points for the
    application after installation. Hence the name.

5. **-trashbin.py**

    As the name tells us, the module represents our file storage functionality, like cleanup policy,
    folder size, displaying its contetns, restoring and so on.

6. **-recycler.py**

    Includes methods for removing files. Appends to an each file a unique HEX uuid to 
    avoid collisions and put down all the file information in a .info file, to provide all the data 
    needed for the restore method.

7. **-helpers.py**

    Auxiliary module with input functions mostly.

8. **-constants.py**

    Holds all the neccessary default values.

9. **-path_graph.py**

    Creates graph from list of paths and removes reduntant ones


