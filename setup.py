from setuptools import setup, find_packages
from os.path import join, dirname

setup(
    name="RMTool",
    version=0.5,
    packages=find_packages(),
    long_description=open(join(dirname(__file__), 'README.md')).read(),
    include_package_data=True,
    entry_points={
        'console_scripts': [

            'rmtr = rmtool.app:remove',
            'rmtbr = rmtool.app:inspect_and_restore'
            ]
        }
    )
