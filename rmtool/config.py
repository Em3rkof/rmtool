#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""The module is responsible for removal process.

* Brief constants description:
    - if starts with "DEFAULT" - store default values, which will be used for 
        configuration dictionary initialization, if the default configuration 
        file won't be found.
"""

import os
import json
import toml
from rmtool import constants

class Config(object):
    """The class is responsible for creating/loading/saving 

    configuration files.    

    *Fields aviable:
        - dictionary - a dictionary, containing all configuration parameters.
        Initializes either from configuration file or with default values,
        if the file can't be found.

    *Methods aviable:
        - create_json_config(path, dictionary) - creates .json configuration 
        file
        - create_toml_config(path, dictionary) - same, but in the TOML format
        - load_defaults(**kwargs) - Initializes configuration dictionary 
        with default values.
        - process_arguments(config, args) - Replaces config dictionary keys if 
        appropriate arguments were specified.
        - load_config - Updates configuration dictionaries with configuration 
        file values. 
             
        """
    def __init__(self):
        self.dictionary = {}
        super(Config, self).__init__()

    def load_defaults(self, 
                      path_to_storage=constants.DEFAULT_PATH_TO_STORAGE,
                      path_to_info_file=constants.DEFAULT_PATH_TO_INFO_FILE,
                      storage_size=constants.DEFAULT_STORAGE_SIZE,
                      ):
        """Initializes config dictionary with default values. """
        self.dictionary = {"bin_config": {}, "recycler_config": {}}
        self.dictionary["bin_config"].update({"storage_size": storage_size,
                                              "policy_max_size": storage_size,
                                              "policy_file_count": None,
                                              "policy_store_time": None,
                                              "info_file_path": path_to_info_file,
                                              "storage_path": path_to_storage
                                             },
                                            )
        self.dictionary["recycler_config"].update({"permanent_mode": False,
                                                   "recursive_mode": False,
                                                   "interactive_mode": False,
                                                   "dry_run_mode": False,
                                                   "silent_mode":False
                                                  },
                                                 )
        return self.dictionary
                       
    def create_json_config(self, path, dictionary):
        """Creates .json config file by specified path filled with dictionary

        passed."""
        if not path.endswith(".json"):
            path = path + ".json"

        with open(path, "w") as f:
            f.write(json.dumps(dictionary))  

    def create_toml_config(self, path, dictionary):
        """Creates .toml config file by specified path filled with dictionary

        passed."""
        if not path.endswith(".toml"):
            path = path + ".toml"

        with open(path, "w") as f:
            f.write(toml.dumps(dictionary))                     

    def load_config(self, path):
        """Updates configuration dictionaries with configuration file values."""
        if os.path.exists(path) and path.endswith(".json"):
            with open(path, "r") as f:
                self.dictionary.update(json.loads(f.read()))
                return self.dictionary
        elif os.path.exists(path) and path.endswith(".toml"):  
            with open(path, "r") as f:
                self.dictionary.update(toml.loads(f.read()))
                return self.dictionary
        else:
            return self.dictionary

    def process_arguments(self, config, args):
        """Replaces config dictionary keys if appropriate arguments were 

        specified."""

        if args.throwtoml:
            self.create_toml_config(args.throwtoml, self.dictionary)
        if args.loadtoml:
            self.load_config(args.loadtoml)
        if args.throwjson:
            self.create_json_config(args.throwjson, self.dictionary)
        if args.loadjson:
            self.load_config(args.loadjson)

        config.update(self.dictionary)
        bin_dict = config["bin_config"]
        recycler_dict = config["recycler_config"]

        if args.throwtoml:
            self.create_toml_config(args.throwtoml, self.dictionary)
        if args.loadtoml:
            self.load_config(args.loadtoml)
        if args.throwjson:
            self.create_json_config(args.throwjson, self.dictionary)
        if args.loadjson:
            self.load_config(args.loadjson)
        if args.storagepath:
            bin_dict["storage_path"] = args.storagepath
            bin_dict["info_file_path"] = os.path.join(args.storagepath, ".info")
        if args.storagesize:
            bin_dict["storage_size"] = args.storagesize

        if (args.maxvolume
                and args.maxvolume > bin_dict["storage_size"]):
            bin_dict["policy_max_size"] = bin_dict["storage_size"]
        elif args.maxvolume:
            bin_dict["policy_max_size"] = args.maxvolume

        if args.filecount:
            bin_dict["policy_file_count"] = args.filecount
        if args.storetime:
            bin_dict["policy_store_time"] = args.storetime
        if args.recursive:
            recycler_dict["recursive_mode"] = args.recursive
        if args.silent:
            recycler_dict["silent_mode"] = args.silent
        if args.dryrun:
            recycler_dict["dry_run_mode"] = args.dryrun
        if args.interactive:
            recycler_dict["interactive_mode"] = args.interactive
        if args.permanent:
            recycler_dict["permanent_mode"] = args.permanent

        config["bin_config"].update(bin_dict)
        config["recycler_config"].update(recycler_dict)

        return config
