import os

#DEAULTS
DEFAULT_PATH_TO_STORAGE = os.path.expanduser("~/TRASHBIN")
DEFAULT_PATH_TO_INFO_FILE = os.path.join(DEFAULT_PATH_TO_STORAGE, ".info")
DEFAULT_STORAGE_SIZE = 10000
DEFAULT_CONFIG_PATH = os.path.join(os.path.dirname(__file__), 
                               ".config.json")
DEFAULT_MAX_FILE_COUNT = 2500
DEFAULT_MAX_STORE_TIME = None
ACCESS_RULE = os.W_OK
CHECK_ACCESS = os.access('{path}', ACCESS_RULE)

#ERR MESSAGES
SUCCESS = (0, "Done.")
ERR_ACCESS_DENIED = (1, "Error while removing {}. Access denied.")
ERR_DIR_NOT_EMPTY = (2, "Error while removing {}. Direcctory not empty.")
ERR_NO_FREE_SPACE = (3, "No free space in the file storage. Aborted")
ERR_TOO_BIG_FILE = (4, "Error while removing {}. File is too big.")
ERR_PATH_NOT_EXISTS = (5, "Path {} doesn't exist.")
ERR_NO_MATCHES = (6, "No matches.")
ERR_UNKNOWN_FILE_FORMAT = (7, "Error while removing {}. Unknown file format.")
ERR_FILE_STORAGE_EMPTY = (8, "File storage is empty.")
ERR_ABORTED = (9, "Aborted.")
