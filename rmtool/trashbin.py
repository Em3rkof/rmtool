#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""The module implements file storage functionality.

* Brief constants description:
    - if starts with "DEFAULT" - store default values for instance creation, if
        no kwargs passed to __init__
    - if starts with "ERR" or "SUCCESS" - a tuple, which first value is 
        operation return code, the second one is the following message

"""

import os
import csv
import shutil
import logging
import datetime

from rmtool.config import Config
from rmtool import constants
from rmtool import helpers


class Trashbin(object):
    """Implements the file storage functionality, like restoring files, policy 

    for cleaning-up and etc.

    *fields aviable:
        - path_to_storage - path to file storage(a new one will be created, if
        doesn't exist)
        - info_file_path - path to file storing information file(a new one will
        be created, if doesn't exist)
        - interactive_mode - ask confirmation before action is done
        - dryrun_mode - working initation without any changes being done
        - storage_size - file storage size(in bytes)
        ***if fields above are set to -1, automatically removal won't work.***
        - policy_max_size - maximum file storage size. If exceeded, the file 
        storage will be emptied automatically. 
        - policy_max_file_count - maximum file amount in the file  storage. If 
        exceeded, the file storage will be emptied automatically. 
        - policy_max_store_time - if file is stored more than field value
        specified, it will be removed permanently.

    *methods aviable:
        - apply_policy(path) - general method, launches definite methods 
        according to the policy_* fields values.
        - apply_file_count_policy() - read policy_max_file_count field descr.
        - apply_store_time_policy() - read policy_max_store_time field descr.
        - apply_max_size_policy() - read policy_max_size field descr.
        - show_content() - displays every 10 files in the file storage on the 
        screen. Clen up and restoring facilities can be activated.
        - restore(name) - restores files by name passed. Returns list of 
        (source, dest) tuple of all the restored files.
        - clean() - totally cleans file storage.  
    """
    def __init__(self, 
                 path_to_storage=constants.DEFAULT_PATH_TO_STORAGE,
                 info_file_path=constants.DEFAULT_PATH_TO_INFO_FILE,
                 dryrun_mode=False, 
                 interactive_mode=False,
                 storage_size=constants.DEFAULT_STORAGE_SIZE,
                 policy_max_size=constants.DEFAULT_STORAGE_SIZE,
                 policy_max_file_count=constants.DEFAULT_MAX_FILE_COUNT,
                 policy_max_store_time=constants.DEFAULT_MAX_STORE_TIME,
                ):

        super(Trashbin, self).__init__()
        self.path_to_storage = os.path.expanduser(path_to_storage)
        self.info_file_path = os.path.join(self.path_to_storage, 
                                           ".info")

        if not os.path.exists(self.path_to_storage):
            os.makedirs(self.path_to_storage) 
        with open(self.info_file_path, "a+") as f:
            pass  

        self.interactive_mode = interactive_mode
        self.dryrun_mode = dryrun_mode
        self.storage_size = storage_size
        self.policy_max_size = policy_max_size
        self.policy_max_file_count = policy_max_file_count
        self.policy_max_store_time = policy_max_store_time

    def apply_policy(self):
        """Calls specific cleaning-up policy if required."""
        if self.policy_max_file_count is not None:
            self.apply_file_count_policy()          
        if self.policy_max_size is not None:
            self.apply_max_size_policy()
        if (self.policy_max_store_time is not None
            or not self.check_is_empty()):
            self.apply_store_time_policy()       

    def apply_file_count_policy(self):
        """Cleans up file storage if specified amount of files was exceeded."""
        if self.count_items() >= self.policy_max_file_count:
            if not helpers.ask_confirmation(
                self.interactive_mode, 
                prompt="File storage is going to be"
                       " emptied due to FILECOUNT"
                       " policy. Confirm?"
            ):
                return 
            self.clean()

    def apply_max_size_policy(self):
        """Cleans up file storage if folder amount exceedes specified value."""
        if helpers.get_file_size(self.path_to_storage) >= self.policy_max_size: 
            if not helpers.ask_confirmation(
                self.interactive_mode, 
                prompt="File storage is going to be"
                       " emptied due to FILECOUNT"
                       " policy. Confirm?"
            ):
                return 
            self.clean()
            
    def apply_store_time_policy(self):
        """Removes all the files which are in the folder more than 

        specified time.
        """
        if not helpers.ask_confirmation(
            self.interactive_mode, 
            prompt="Some files might be removed according" 
                   " to the STORETIME policy. Confirm?"
        ):
            return
        info = []
        count = 0
        with open(self.info_file_path, "r+w") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                delta = (datetime.datetime.now() 
                         - datetime.datetime.strptime(
                            line[0],
                            "%Y-%m-%d, %H:%M:%S:%f"))
                seconds = delta.seconds
                hours = seconds / float(3600)
                
                if hours > self.policy_max_store_time:
                    path = os.path.join(self.path_to_storage,
                                        "{name}.{id}".format(
                                           name=line[1], 
                                           id=line[2]))
                                                             
                    if line[4] == "Directory": 
                        shutil.rmtree(path)
                    else:
                        os.remove(path)
                    count += 1
                    continue
                info.append(line)

            logging.info("{} files were removed according to the" 
                         " STORETIME policy".format(count))
            f.seek(0)
            f.truncate()
            writer = csv.writer(f, delimiter='|')

            for item in info:
                writer.writerow(item)

    def show_content(self):
        """Displays every 10 items in the folder. Use keys, specified in the

        prompt, to navigate over the files, activate file restoring facility or

        clean the file storage.
        """
        result_data = []
        lower_index = 0
        with open(self.info_file_path, "r") as f:
            info = []
            data = csv.reader(f, delimiter="|")

            for line in data:
                source_path = line[3]
                dest_path = os.path.join(
                    self.path_to_storage,
                    "{}.{}".format(line[1], line[2]))
                info.append(line)

            if self.check_is_empty():
                logging.info(constants.ERR_FILE_STORAGE_EMPTY[1])
                result_data.append((constants.ERR_FILE_STORAGE_EMPTY, 
                                    None, 
                                    None,
                                    )
                                   )
                return [result_data]

            while True:
                if lower_index + 10 > len(info):
                    upper_index = len(info)
                else:
                    upper_index = lower_index + 10

                for lower_index in xrange(lower_index, upper_index):
                    source_name = info[lower_index][1]
                    removal_time = info[lower_index][0]
                    file_type = info[lower_index][4]

                    if file_type== "File":
                        color = '\033[92m'
                    elif file_type == "Directory":
                        color = '\033[94m'
                    elif file_type == "Link":
                        color = '\033[93m'
                    else:
                        color = '\033[91m'
                    print "Name: {} | Type:{}{}{}" \
                        "| Removal time: {}".format(source_name, color,
                                                    file_type, '\033[0m',
                                                    removal_time)
                
                command = helpers.get_command()
                    
                if command == 'q':
                    result_data.append((constants.SUCCESS, None, None))
                    return [result_data]
                elif command == 'm':
                    if (lower_index == len(info) - 1
                            and not upper_index % 10 == 0):
                        lower_index -= upper_index % 10
                    elif lower_index == len(info) - 1:
                        lower_index -= 10
                    lower_index += 1
                elif command == 'c':
                    return [self.clean()]
                elif command == 'r':
                    prompt = "Enter the names of the files you want to restore:"
                    names = [name for name in raw_input(prompt).split()]
                    return self.restore(names)
                else:
                    if (lower_index == len(info) - 1 
                            and not upper_index % 10 == 0):
                        lower_index -= upper_index % 10 - 1
                    else:
                        lower_index -= 9
                    if not lower_index == 0:
                        lower_index -= 10
        

    def silent_restore(self, names):
        """Restores by name in file storage without any I/O"""
        info = []
        exit_status = constants.SUCCESS
        files_info = []
        files_to_exclude = []

        with open(self.info_file_path, "r+w") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                source_name = line[1]
                dest_name = "{name}.{id}".format(
                    name=source_name, 
                    id=line[2])
                if not dest_name in names:
                    files_info.append(line)
        

        with open(self.info_file_path, "r+w") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                source_name = line[1]
                source_path = line[3]
                dest_name = "{name}.{id}".format(
                    name=source_name, 
                    id=line[2])
                if dest_name in names:
                    dest_path = os.path.join(self.path_to_storage,
                                             dest_name)         
                    info.append((exit_status, source_path, dest_path))
                    logging.info("Done")
                    
                    while os.path.exists(source_path):
                            source_path = source_path + '1'

                    if not self.dryrun_mode:
                        os.renames(dest_path, source_path) 

            f.seek(0)
            f.truncate() 
            writer = csv.writer(f, delimiter='|') 

            for file in files_info:                    
                writer.writerow(file)

        return [info]
            
                    
    def restore(self, names):
        """Displays all the file names which match user input and asks for 

        exact choice. Returns list of (source path, destination path) of restored

        files.
        """
        info = []
    
        exit_status = constants.SUCCESS

        for name in names:
            item_index = 0
            files = {}
            files_info = []
            
            with open(self.info_file_path, "r+w") as f:
                data = csv.reader(f, delimiter="|")
                for line in data:
                    source_name = line[1]
                    source_path = line[3]
                    dest_name = "{name}.{id}".format(name=source_name, 
                                                     id=line[2])

                    files_info.append(line)
                    if source_name == name:
                        item_info = []
                        item_index += 1
                        print "{} - {}".format(item_index, dest_name)
                        item_info.append(dest_name)
                        item_info.append(source_path)
                        files.update({str(item_index): item_info})

                if item_index == 0:
                    exit_status = constants.ERR_NO_MATCHES
                    logging.info(constants.ERR_NO_MATCHES[1])
                    info.append((exit_status, None, None))
                    continue
                while True:
                    selected_item_index = helpers.select_index(item_index)
                    if selected_item_index != -1:
                        break

                item_info = files.get(str(selected_item_index))
                item_dest_name = item_info[0]
                item_source_path = item_info[1]
                source_path = self._solve_name_conflict(item_source_path)
                dest_path = os.path.join(self.path_to_storage, 
                                         item_dest_name)
                logging.info("Restoring file {}...".format(name))

                if not self.dryrun_mode:
                    os.rename(dest_path, source_path)
                    f.seek(0)
                    f.truncate()
                    writer = csv.writer(f, delimiter='|')        

                    for file in files_info:
                        if ("{name}.{id}".format(name=file[1], id=file[2]) 
                                == item_dest_name):
                            continue
                        writer.writerow(file)

                logging.info("Done")

                info.append((exit_status, source_path, dest_path))

        return [info]

    def _solve_name_conflict(self, prev_path):
        """Offers to rename your file/foler if name conflict occured 

        during restoring. Returns modified path.
        """
        if not os.path.exists(prev_path):
            return prev_path
        logging.info("The file with the name {} already exists." 
                     "Select another name for the root file".format(
            os.path.basename(prev_path)))
        while True:
            new_name = raw_input("Type a new root file name: ")
            head, tail = os.path.split(prev_path)
            if not os.path.exists(os.path.join(head, new_name)):
                break
            logging.info("File with this name already exists.")
        return os.path.join(os.path.join(head, new_name))

    def check_is_empty(self):
        isempty = True
        for line in os.listdir(self.path_to_storage):
            if line.startswith(".info"):
                continue
            isempty = False
            break
        if isempty:
            return True
        else:
            return False

    def clean(self):
        info = []
        logging.info("Cleaning up...")

        if self.dryrun_mode:
            info.append((constants.SUCCESS, []))
            return info

        with open(self.info_file_path, "r+w") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                path = os.path.join(self.path_to_storage, "{name}.{id}".format(
                                        name=line[1],
                                        id=line[2]),
                                        )
                if line[4] == "Directory":
                    shutil.rmtree(path)                     
                else:
                    os.remove(path)                
            f.seek(0)
            f.truncate()

        logging.info("File storage is empty")
        info.append((constants.SUCCESS, None, None))
        return info

    def count_items(self):
        line_count = 0
        with open(self.info_file_path, "r") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                line_count += 1
        return line_count

    def get_content(self):
        contents = []
        with open(self.info_file_path, "r") as f:
            data = csv.reader(f, delimiter="|")
            for line in data:
                source_path = line[3]
                dest_name = "{name}.{id}".format(name=line[1],
                                                 id=line[2],
                                                )
                contents.append((source_path, dest_name))
        if not contents:
            contents.append((None, None))
        return contents

    def get_size(self):
        return helpers.get_file_size(self.path_to_storage)
