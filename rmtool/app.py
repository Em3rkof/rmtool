#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import os
import logging

from rmtool import constants
from rmtool.config import Config
from rmtool.trashbin import Trashbin
from rmtool.recycler import Recycler
from rmtool.argparser import Argparser

def remove():
    """Entry point for removing application part. """
    info = []
    config = Config()

    if not config.load_config(constants.DEFAULT_CONFIG_PATH):
        config.load_defaults()
        config.create_json_config(constants.DEFAULT_CONFIG_PATH, 
                                  config.load_defaults(
                                    path_to_storage=constants.DEFAULT_PATH_TO_STORAGE,
                                    path_to_info_file=constants.DEFAULT_PATH_TO_INFO_FILE,
                                    storage_size=constants.DEFAULT_STORAGE_SIZE
                                  ),
                                 )
                                                            
    parser = Argparser()
    args = parser.parse_remove_args()
    config_dictionary = config.process_arguments(config.dictionary, args)
    bin_dict = config_dictionary["bin_config"]
    recycler_dict = config_dictionary["recycler_config"]

    if config_dictionary["recycler_config"]["silent_mode"]:
        logging.basicConfig(level=logging.CRITICAL, format='%(message)s')
    else:
        logging.basicConfig(level=logging.INFO, format='%(message)s')

    trashbin = Trashbin(
        path_to_storage=bin_dict["storage_path"],
        info_file_path=bin_dict["info_file_path"],
        dryrun_mode=recycler_dict["dry_run_mode"], 
        interactive_mode=recycler_dict["interactive_mode"],
        storage_size=bin_dict["storage_size"],
        policy_max_size=bin_dict["policy_max_size"],
        policy_max_file_count=bin_dict["policy_file_count"],
        policy_max_store_time=bin_dict["policy_store_time"],
    )

    trashbin.apply_policy()

    recycler = Recycler(
        path_to_storage=bin_dict["storage_path"],
        info_file_path=bin_dict["info_file_path"],
        dryrun_mode=recycler_dict["dry_run_mode"],  
        interactive_mode=recycler_dict["interactive_mode"],
        recursive_mode=recycler_dict["recursive_mode"],
        permanent_mode=recycler_dict["permanent_mode"],
        storage_size=bin_dict["storage_size"],
    )

    regex = None

    if args.regex:
        regex = args.regex

    info = recycler.launch_rm(args.path, regex)
    exit_code = 0

    for path in info:
        for result, source, dest in path:
            code = result[0]
            msg = result[1]
            if code > exit_code:
                exit_code = code
            logging.info("Removing {}...".format(source))
            logging.info(msg.format(source))

    return exit_code

def inspect_and_restore():
    """Entry point for the inspect and restore application part. """
    info = []
    config = Config()
    parser = Argparser()
    args = parser.parse_inspect_args()

    if not config.load_config(constants.DEFAULT_CONFIG_PATH):
        config.load_defaults()
        config.create_json_config(
            constants.DEFAULT_CONFIG_PATH, config.load_defaults(
                path_to_storage=constants.DEFAULT_PATH_TO_STORAGE,
                path_to_info_file=constants.DEFAULT_PATH_TO_INFO_FILE,
                storage_size=constants.DEFAULT_STORAGE_SIZE,
            )
        )

    logging.basicConfig(level=logging.INFO, format='%(message)s')
    bin_dict = config.dictionary["bin_config"]
    recycler_dict = config.dictionary["recycler_config"]

    if args.dryrun:
        recycler_dict["dry_run_mode"] = True

    trashbin = Trashbin(
        path_to_storage=bin_dict["storage_path"],
        info_file_path=bin_dict["info_file_path"],
        dryrun_mode=recycler_dict["dry_run_mode"], 
        interactive_mode=recycler_dict["interactive_mode"],
        storage_size=bin_dict["storage_size"],
        policy_max_size=bin_dict["policy_max_size"],
        policy_max_file_count=bin_dict["policy_file_count"],
        policy_max_store_time=bin_dict["policy_store_time"],
    )

    trashbin.apply_policy()

    recycler = Recycler(
        path_to_storage=bin_dict["storage_path"],
        info_file_path=bin_dict["info_file_path"],
        dryrun_mode=recycler_dict["dry_run_mode"],  
        interactive_mode=recycler_dict["interactive_mode"],
        recursive_mode=recycler_dict["recursive_mode"],
        permanent_mode=recycler_dict["permanent_mode"],
        storage_size=bin_dict["storage_size"],
    )

    info = trashbin.show_content()
    exit_code = 0

    for path in info:
        for result, source, dest in path:
            code = result[0]
            msg = result[1]
    
            if code > exit_code:
                exit_code = code

    return exit_code
    

if __name__ == '__main__':
    remove()
   # inspect_and_restore()

