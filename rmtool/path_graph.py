#!/usr/bin/python2.7
#-*- coding: utf-8 -*-

"""Holds Path_Graph class which is responsible for creating 
graphs from paths and resolving them in specific way """

from os import path as P
from collections import defaultdict

class PathGraph(object):
    """Implements functionality to build and resolve graph"""
    def __init__(self, paths):
        self.graph = defaultdict(set)
        self.paths = paths
        self.leafs = []
        self.output = []

    def make_graph(self):
        """Makes graph from list of paths given"""
        for path in self.paths:
            prev = ""
            split = path.split("/")[1:]
            for index, section in enumerate(split[:-1]):
                new = P.join(prev, section)
                if index == len(split) - 2:
                    self.leafs.append(P.join(new, split[index + 1]))
                self.graph[new].add(split[index + 1])
                prev = new

    def define_reduntant_paths(self, key=P.expanduser("~")[1:]):
        """Returns a new list which consists only of common ancestors or
        nodes themselves if the  common ancestor is /home/user/ path. e.g
        [~/5, ~/5/6, ~/5/6/7, ~/4] will return [~/5, ~/4]. """
        for value in self.graph[key]:
            path = P.join(key, value)
            if path in self.leafs:
                self.output.append("/" + path)
                continue
            else:
                self.define_reduntant_paths(key=path)
            path = ""
        return self.output

    def print_graph(self):
        for k, v in self.graph.iteritems():
            print "Key: {} Value: {}".format(k, v)






