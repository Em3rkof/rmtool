"""Small utilite for files removing with file storage support.

Included modules:
	-app - entry point
	-trashbin - represents file storgae functionality
	-recycler - responsible for removing objects
	-config - manages configuration files
	-helpers - some auxiliary functions
	-constants - most used values 
	-path_graph - working with list of paths
"""
