"""Module with some auxiliary functions.

*constants aviable:
    Console prompts(messages) only.

*functions aviable:
    - ask_confirmation(*args, **kwargs) - takes user input(y/n), returns 
    true/false appropriately.

    - select_index(*args, **kwargs) - returns user input or -1.

    - get_command() - returns user input if it's presented in the command list

    - check_contains_files(path) - self-explanatory

    - check_contains_files(path) - self-explanatory

    - get_file_size(path) - self-explanatory

"""

import os

CONSOLE_PROMPT_ASK = "Are you sure?"
CONSOLE_PROMPT_SELECT = "Select the file you want to restore: "
CONSOLE_PROMPT_NAVIGATION = "m or l - navigate," \
                             "c - cleanup, r - restore, q - exit:"


def ask_confirmation(active, prompt=CONSOLE_PROMPT_ASK, default=False):
    """Just takes user input and returns True if y(yes) was inputted, otherwise

    returns False.
    """
    if not active:
        return True

    answer = raw_input(prompt)
    yes = ["Y", "YES"] 
    no = ["N", "NO"]
    yes = [st.upper() for st in yes]
    no = [st.upper() for st in no]
    answer = answer.upper()

    if answer in yes:
        return True
    elif answer in no:
        return False
    else:
        return default


def select_index(index, prompt=CONSOLE_PROMPT_SELECT, default=-1):
    """Returns input or -1 if it's out of range. """
    answer = raw_input(prompt)

    if int(answer) - 1 in range(index):
        return int(answer)
    else:
        return default


def get_command():
    """Returns one of the commands in the list or asks to enter again if input

    is incorrect.
    """
    while True:
        command = raw_input(CONSOLE_PROMPT_NAVIGATION)
        if command in ['q', 'm', 'c', 'l', 'r']:
            break
    return command


def check_contains_files(path):
    for data in os.walk(path):
        if data[2]:
            return True
        else:
            return False


def check_contains_dirs(path):
    for data in os.walk(path):
        if data[1]:
            return True
        else:
            return False


def get_file_size(path):
    total_size = 0.0
    
    if not os.path.isdir(path) and not path.startswith('.info'):
        total_size = os.path.getsize(path)
        return float(total_size / 1000000.0)
    else:
        for data in os.walk(path):
            for f in data[2]:
                if not f.startswith('.info'):
                    fp = os.path.join(data[0], f)
                    total_size += os.path.getsize(fp)
        return float(total_size / 1000000.0)   
