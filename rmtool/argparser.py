#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

import argparse


class Argparser(object):
    """Class for parsing console arguments."""
    def __init__(self):
        self.remove_parser = argparse.ArgumentParser()
        self.group = self.remove_parser.add_mutually_exclusive_group()
        self.remove_parser.add_argument(
            "path",
            type=str,
            nargs="+",
            help="Path to the file needs to be processed"
        )
        self.remove_parser.add_argument(
            "--regex",
            type=str,
            help="Directory cleaning-up by regex passed"
        )
        self.remove_parser.add_argument(
            "-r",
            "--recursive",
            help="Removes directories and their content",
            action="store_true"
        )
        self.remove_parser.add_argument(
            "-p",
            "--permanent",
            help="All the files will be removed permanently",
            action="store_true"
        )
        self.remove_parser.add_argument(
            "--storagesize",
            type=int,
            help="Maximum file storage size"
        )
        self.remove_parser.add_argument(
            "--storagepath",
            type=str,
            help="Path to the file storage"
        )
       	self.remove_parser.add_argument(
            "--throwtoml",
            type=str,
            help="Set path where custom .toml configuration file will be"  
            	 "created."
        )
       	self.remove_parser.add_argument(
            "--loadtoml",
            type=str,
            help="Set path to your own .toml configure file to overwrite" 
            	  "base configuration"
        )
       	self.remove_parser.add_argument(
            "--throwjson",
            type=str,
            help="Set path where custom .json configuration file will be"  
            	 "created."
        )
       	self.remove_parser.add_argument(
            "--loadjson",
            type=str,
            help="Set path to your own .json configure file to overwrite" 
            	  "base configuration"
        )
        self.remove_parser.add_argument(
            "--filecount",
            type=int,
            help="Maximum amount of files trashbin can contain"
        )
        self.remove_parser.add_argument(
            "--maxvolume",
            type=int,
            help="When this size will be reached, file storage will be"
            	 "emptied automatically"
        )
        self.remove_parser.add_argument(
            "--storetime",
            type=int,
            help="Maximum term(in minutes) after that the file will be" 
                 "removed"
        )
        self.group.add_argument(
            "-s",
            "--silent",
            help="Removes files without any confirmation and information" 
                 "provided",
            action="store_true"
        )
       	self.remove_parser.add_argument(
            "-d",
            "--dryrun",
            help="Imitates application work without any changes done.",
            action="store_true"
        )
       	self.group.add_argument(
   			"-i",
            "--interactive",
            help="Asks for permission before any action.",
            action="store_true"
        )

       	self.inspect_parser = argparse.ArgumentParser()
       	self.inspect_parser.add_argument(
            "-d",
            "--dryrun",
            help="Imitates application work without any changes done.",
            action="store_true"
        )

    def parse_remove_args(self):
        return self.remove_parser.parse_args()

    def parse_inspect_args(self):
        return self.inspect_parser.parse_args()
               