#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

"""The module is responsible for removal process.

* Brief constants description:
    - if starts with "DEFAULT" - store default values for instance creation, if
        no kwargs passed to __init__
    - if starts with "ERR" or "constants.SUCCESS" - a tuple, which first value is 
        operation return code, the second one is the following message
"""

import os
import re
import csv
import uuid
import logging
import datetime
import copy_reg
import types
import multiprocessing as mp
from functools import partial

from rmtool.path_graph import PathGraph 
from rmtool.config import Config
from rmtool import constants
from rmtool import helpers

LOCK = mp.RLock()

class Recycler(object): 
    """Implements methods for file removing and colletcts necessary information 

    for the restore and browse functions

    *fields aviable:
        - path_to_storage - path to file storage(a new one will be created, if
        doesn't exist)
        - info_file_path - path to file storing information file(a new one will
        be created, if doesn't exist)
        - permanent_mode - permanent file removal
        - interactive_mode - ask confirmation before action is done
        - dryrun_mode - working initation without any changes being done
        - storage_size - file storage size(in bytes)
        - recursive_mode - removes all the folder's content

    *methods aviable:
        - remove_by_regexp(regexp, subtree_path) - tries to remove files in the 
        subtree by regexp passed
        - launch_rm - launches multiprocessing removal
        - remove(path) - general method, launches definite methods according to 
        the file type and recycler mode
        - remove_dir(path) - tries to remove empty directory. 
        - remove_recursive(path) - tries to remove directory and its contents
        ***All the methods above return error tuple, source file path and 
        destination path(None if permanent flag is False)***
        - _save_info(path, label) - saves file information(source path, name and 
        etc.) to information file.
    """
    def __init__(self, 
                 path_to_storage=constants.DEFAULT_PATH_TO_STORAGE,
                 info_file_path=constants.DEFAULT_PATH_TO_INFO_FILE,
                 dryrun_mode=False, 
                 interactive_mode=False,
                 recursive_mode=False,
                 permanent_mode=False,
                 storage_size=constants.DEFAULT_STORAGE_SIZE,
                ):
        super(Recycler, self).__init__()

        self.path_to_storage = os.path.expanduser(path_to_storage)
        self.info_file_path = os.path.join(self.path_to_storage, 
                                           ".info")

        if not os.path.exists(self.path_to_storage):
            os.makedirs(self.path_to_storage) 
        with open(self.info_file_path, "a+") as f:
            pass  

        self.permanent_mode = permanent_mode
        self.interactive_mode = interactive_mode
        self.dryrun_mode = dryrun_mode
        self.storage_size = storage_size
        self.recursive_mode = recursive_mode

    def remove_by_regexp(self, regexp, subtree_path):
        """Tries to remove a subtree content by the regex passed.

        Returns errors tuple, source and destination file path(None if permanent

        flag is False).
        """  
        info = []
        if not os.path.exists(subtree_path):
            info.append((constants.ERR_PATH_NOT_EXISTS, subtree_path, None))
            return info
        for line in os.listdir(subtree_path):
            if re.findall(regexp, line):
                info.extend(self.remove(os.path.join(subtree_path, line)))
        if not info:
            info.append((constants.ERR_NO_MATCHES, subtree_path, None))
            return info
        else:
            return info

    def _pickle_method(m):
        if m.im_self is None:
            return getattr, (m.im_class, m.im_func.func_name)
        else:
            return getattr, (m.im_self, m.im_func.func_name)

    copy_reg.pickle(types.MethodType, _pickle_method)

    def launch_rm(self, paths, regexp=None):
        """Launches multiprocessing removal """
        paths = [os.path.abspath(item) for item in paths]
        pg = PathGraph(paths)
        pg.make_graph()
        paths = pg.define_reduntant_paths()
        cpu_count = mp.cpu_count() * 2
        pool_size = cpu_count if len(paths) > cpu_count else len(paths)
        pool = mp.Pool(processes=pool_size)

        if regexp:
            result = pool.map(
                partial(self.remove_by_regexp, regexp),
                paths
            )
        else:
            result = pool.map(self.remove, paths)

        pool.close()
        pool.join()

        return result

    def remove(self, path):
        """Method which calls specific removing methods according to the file 

        type and arguments passed.  Returns errors tuple, source and destination 

        file path(None if permanent flag is False).
        """
        info = []
        if not os.path.exists(path):
            info.append((constants.ERR_PATH_NOT_EXISTS, path, None))
            return info
        if not self.permanent_mode:
            if helpers.get_file_size(path) > self.storage_size:
                info.append((constants.ERR_TOO_BIG_FILE, path, None))
                return info
            if (helpers.get_file_size(self.path_to_storage) 
                    + helpers.get_file_size(path)  > self.storage_size):
                info.append((constants.ERR_NO_FREE_SPACE, path, None))
                return info
        
        if os.path.isfile(path) or os.path.islink(path):
            info.extend(self.remove_file(path))
            return info
        elif os.path.isdir(path) and not self.recursive_mode:
            info.extend(self.remove_dir(path))
            return info
        elif os.path.isdir(path) and self.recursive_mode:
            info.extend(self.remove_recursive(path))
            return info
        else:
            info.append(constants.ERR_UNKNOWN_FILE_FORMAT, path, None)
            return info

    def remove_file(self, 
                    path, 
                    dest=None,
                    append_id=True,
                    _save_info=True,
                    ):
        """Tries to remove a single file.  Returns errors tuple, source and 

        destination file path(None if permanent flag is False).
        """
        if dest is None:
            dest = self.path_to_storage

        info = []
        label = uuid.uuid4()

        if append_id:
            newroot = os.path.join(dest, 
               "{name}.{id}".format(
                    name=os.path.basename(path),
                    id=str(label)
                )
            )
        else:
            newroot= os.path.join(dest, os.path.basename(path)) 

        if not os.access(path, constants.ACCESS_RULE):
            info.append((constants.ERR_ACCESS_DENIED, path, None))
            return info
        if not helpers.ask_confirmation(
                self.interactive_mode, 
                "Are you sure to delete the {} file?".format(path)):
            info.append((constants.ERR_ABORTED, path, None))
            return info

        if not self.dryrun_mode:
            if self.permanent_mode:
                os.remove(path)
            else:
                if _save_info:
                    self._save_info(path, label)
                os.rename(path, newroot)

        if self.permanent_mode:
            info.append((constants.SUCCESS, path, None))
            return info
        else:
            info.append((constants.SUCCESS, path, newroot))
            return info

    def remove_dir(self,
                   path, 
                   dest=None,
                   _save_info=True,
                   ):
        """Tries to remove a single empty directory, throws an error if 

        directory is not empty, or user has no W permission.
        """
        if dest is None:
            dest = self.path_to_storage

        info = []
        label = uuid.uuid4()       
        newroot = os.path.join(dest, 
                               "{name}.{id}".format(
                                    name=os.path.basename(path),
                                    id=str(label)
                                )
                               ) 
        if not os.access(path, constants.ACCESS_RULE):
            info.append((constants.ERR_ACCESS_DENIED, path, None))
            return info

        if (helpers.check_contains_files(path) 
                or helpers.check_contains_dirs(path)):
            info.append((constants.ERR_DIR_NOT_EMPTY, path, None))
            return info

        if not self.dryrun_mode:
            if not helpers.ask_confirmation(
                        self.interactive_mode, 
                        "Are you sure to delete the {} folder?".format(path)
                    ):
                info.append((constants.ERR_ABORTED, path, None))
                return info
            if not self.permanent_mode:
                if _save_info:
                    self._save_info(path, label)    
                os.mkdir(newroot)
            os.rmdir(path)   

        if self.permanent_mode:
            info.append((constants.SUCCESS, path, None))
            return info
        else:
            info.append((constants.SUCCESS, path, newroot))       
            return info   
    

    def remove_recursive(self, path):
        """Tries to remove a single empty or not empty deirectory, if the -r

        argument was passed. Returns errors tuple, source and destination file 

        path(None if permanent flag is False).
        """
        info = []
        newroot = None
        newpath = None

        if not helpers.ask_confirmation(
                    self.interactive_mode, 
                    "Are you sure to delete the {} folder?".format(path)
               ):
            info.append((constants.ERR_ABORTED, path, None))
            return info

        if not self.permanent_mode:
            info.extend(self._copy_folder_tree(path))
            newroot = info[-1][2]

        for root, dirs, files in os.walk(path, topdown=True):
            for name in files:
                if not self.permanent_mode:
                    if root == path:
                        newpath = os.path.join(
                            newroot, 
                            root.replace(path, ""))
                    else:
                        newpath = os.path.join(
                            newroot, 
                            root.replace(path + '/', "")
                        )
                info.extend(self.remove_file( 
                    os.path.join(root, name), 
                    dest=newpath,
                    append_id=False,
                    _save_info=False,
                    )
                )

        if not self.dryrun_mode:
            self._remove_folder_tree(path)
        
        return info

    def _copy_folder_tree(self, path, dest=None, _save_info=True):
        """Tries to copy folder tree with "path" top=level folder to
        dest. """
        if dest is None:
            dest = self.path_to_storage
        info = []
        if not os.access(path, constants.ACCESS_RULE):
            info.append((constants.ERR_ACCESS_DENIED, path, None))
            return info

        label = uuid.uuid4() 
  
        newroot = os.path.join(dest, 
                               "{name}.{id}".format(
                                    name=os.path.basename(path),
                                    id=str(label)))
        if not self.dryrun_mode:
            if _save_info:
                self._save_info(path, label)
            os.mkdir(newroot)

        for root, dirs, _ in os.walk(path, topdown=True):
            for name in dirs:
                if not os.access(os.path.join(root, name), 
                                 constants.ACCESS_RULE):
                    info.append((constants.ERR_ACCESS_DENIED,
                                 os.path.join(root, name),
                                 None))
                    continue
                if not self.dryrun_mode:
                    if root == path:
                        newpath = os.path.join(
                            newroot, 
                            root.replace(path, ""))
                    else:
                        newpath = os.path.join(
                            newroot, 
                            root.replace(path + '/', ""))
                    os.mkdir(os.path.join(newpath, name))

        info.append((constants.SUCCESS, path, newroot))
        return info

    def _remove_folder_tree(self, path):
        """Tries to remove folder tree with "path" top-level folder. """
        for root, dirs, files in os.walk(path, topdown=False):
                for name in dirs:
                    dest = os.path.join(root, name)
                    if not os.access(dest, constants.ACCESS_RULE):
                        continue
                    if (not helpers.check_contains_dirs(dest) 
                            and not helpers.check_contains_files(dest)):
                        os.rmdir(dest)

        if (not helpers.check_contains_dirs(path) 
                and not helpers.check_contains_files(path)):
            os.rmdir(path)

    def _save_info(self, path, label):
        """Writes all the necessary data(removal time, file name, it's previous

        path, etc.) in the specific file within file storage folder to provide an 

        information to restore and browse functions.
        """
        with open(self.info_file_path, "a+") as f:
            LOCK.acquire()
            info = [
                datetime.datetime.now().strftime("%Y-%m-%d, %H:%M:%S:%f"),
                os.path.basename(path),
                label,
                os.path.abspath(path),
            ]  
            newroot = os.path.join(self.path_to_storage, 
               "{name}.{id}".format(
                    name=os.path.basename(path),
                    id=str(label))) 
            if os.path.isfile(path):
                info.append("File")
            elif os.path.islink(path):
                info.append("Link")
            elif os.path.isdir(path):
                info.append("Directory")
            else:
                info.append("Undef")

            writer = csv.writer(f, delimiter='|')
            writer.writerow(info)
            LOCK.release()
